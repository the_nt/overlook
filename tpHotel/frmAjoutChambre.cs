﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            string tmp;
            int i;
            for (i=0; i<Persistance.getLstHotels().Count; i++)
            {
                tmp = Convert.ToString(((Hotel)Persistance.getLesHotels()[i]).Nom);
                lsthotel.Items.Add(tmp);
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            string tmp;
            int i;
            for (i = 0; i < Persistance.getLstHotels().Count; i++)
            {
                tmp = Convert.ToString(((Hotel)Persistance.getLesHotels()[i]).Nom);
                if (lsthotel.Text == tmp)
                {
                    Persistance.ajouteChambre((int)numeta.Value, txtdes.Text, ((Hotel)Persistance.getLesHotels()[i]).Id);
                    MessageBox.Show("Chambre ajouté", "Ajout chambre", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnanul_Click(object sender, EventArgs e)
        {
            lsthotel.Items.Clear();
            string tmp;
            int i;
            for (i = 0; i < Persistance.getLstHotels().Count; i++)
            {
                tmp = Convert.ToString(((Hotel)Persistance.getLesHotels()[i]).Nom);
                lsthotel.Items.Add(tmp);
            }
            txtdes.Text = "";
            numeta.Value = 0;
        }
    }
}

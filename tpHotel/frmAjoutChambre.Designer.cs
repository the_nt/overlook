﻿namespace tpHotel
{
    partial class frmAjoutChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numeta = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdes = new System.Windows.Forms.TextBox();
            this.lsthotel = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnanul = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numeta)).BeginInit();
            this.SuspendLayout();
            // 
            // numeta
            // 
            this.numeta.Location = new System.Drawing.Point(86, 31);
            this.numeta.Name = "numeta";
            this.numeta.Size = new System.Drawing.Size(139, 20);
            this.numeta.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Étage: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Description";
            // 
            // txtdes
            // 
            this.txtdes.Location = new System.Drawing.Point(86, 73);
            this.txtdes.Name = "txtdes";
            this.txtdes.Size = new System.Drawing.Size(139, 20);
            this.txtdes.TabIndex = 3;
            // 
            // lsthotel
            // 
            this.lsthotel.FormattingEnabled = true;
            this.lsthotel.Location = new System.Drawing.Point(86, 110);
            this.lsthotel.Name = "lsthotel";
            this.lsthotel.Size = new System.Drawing.Size(139, 21);
            this.lsthotel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Hotel";
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(8, 166);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 6;
            this.btnsave.Text = "Enregistrer";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnanul
            // 
            this.btnanul.Location = new System.Drawing.Point(90, 166);
            this.btnanul.Name = "btnanul";
            this.btnanul.Size = new System.Drawing.Size(75, 23);
            this.btnanul.TabIndex = 7;
            this.btnanul.Text = "Annuler";
            this.btnanul.UseVisualStyleBackColor = true;
            this.btnanul.Click += new System.EventHandler(this.btnanul_Click);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(171, 166);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(75, 23);
            this.btnclose.TabIndex = 8;
            this.btnclose.Text = "Fermer";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // frmAjoutChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 201);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btnanul);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lsthotel);
            this.Controls.Add(this.txtdes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numeta);
            this.Name = "frmAjoutChambre";
            this.Text = "frmAjoutChambre";
            this.Load += new System.EventHandler(this.frmAjoutChambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numeta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numeta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtdes;
        private System.Windows.Forms.ComboBox lsthotel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnanul;
        private System.Windows.Forms.Button btnclose;
    }
}
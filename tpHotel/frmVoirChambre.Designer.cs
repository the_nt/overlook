﻿namespace tpHotel
{
    partial class frmVoirChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstchambre = new System.Windows.Forms.DataGridView();
            this.btnclose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lstchambre)).BeginInit();
            this.SuspendLayout();
            // 
            // lstchambre
            // 
            this.lstchambre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstchambre.Location = new System.Drawing.Point(45, 40);
            this.lstchambre.Name = "lstchambre";
            this.lstchambre.Size = new System.Drawing.Size(705, 318);
            this.lstchambre.TabIndex = 0;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(45, 385);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(705, 53);
            this.btnclose.TabIndex = 1;
            this.btnclose.Text = "Fermer";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmVoirChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.lstchambre);
            this.Name = "frmVoirChambre";
            this.Text = "frmVoirChambre";
            this.Load += new System.EventHandler(this.frmVoirChambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstchambre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lstchambre;
        private System.Windows.Forms.Button btnclose;
    }
}
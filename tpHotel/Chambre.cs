﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre { 

        private int id;
        private int idhotel;
        private int etage;
        private string desc;
        private int cat;

        public Chambre(int Unid, int Unidhotel, int Unetage, string Unedesc,int Uncodecat)
        {
            id = Unid;
            idhotel = Unidhotel;
            etage = Unetage;
            desc = Unedesc;
            cat = Uncodecat;
        }


        public int Id { get => id; set => id = value; }
        public int Idhotel { get => idhotel; set => idhotel = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Desc { get => desc; set => desc = value; }
        public int Cat { get => cat; set => cat = value; }
        
        
        
    }
}
